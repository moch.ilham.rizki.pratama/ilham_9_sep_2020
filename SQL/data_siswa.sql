/*
SQLyog Enterprise v12.4.3 (64 bit)
MySQL - 10.4.14-MariaDB : Database - data_siswa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`data_siswa` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `data_siswa`;

/*Table structure for table `kecamatan` */

DROP TABLE IF EXISTS `kecamatan`;

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(20) NOT NULL,
  `id_kota` int(11) NOT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `id_kota` (`id_kota`),
  CONSTRAINT `kecamatan_ibfk_1` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

/*Data for the table `kecamatan` */

insert  into `kecamatan`(`id_kecamatan`,`nama_kecamatan`,`id_kota`) values 
(1,'Bandung Timur',1),
(2,'Cimahi Utara',2),
(3,'Padalarang',3),
(4,'Cimahi Tengah',2),
(5,'Antapani',1),
(6,'Lembang',3),
(7,'Cimahi Selatan',2),
(8,'Batujajar',3);

/*Table structure for table `kota` */

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `id_kota` int(10) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `kota` */

insert  into `kota`(`id_kota`,`nama_kota`) values 
(1,'Kota Bandung'),
(2,'Kota Cimahi'),
(3,'Kab. Bandung Barat');

/*Table structure for table `siswa` */

DROP TABLE IF EXISTS `siswa`;

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `nama_siswa` varchar(30) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_siswa`),
  KEY `id_kecamatan` (`id_kecamatan`),
  KEY `id_kota` (`id_kota`),
  CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `siswa` */

insert  into `siswa`(`id_siswa`,`nama_siswa`,`id_kota`,`id_kecamatan`,`alamat`) values 
(1,'Agus',1,1,'Alamat Siswa'),
(2,'Budi',2,2,'Alamat Siswa'),
(3,'Nana',3,3,'Alamat Siswa'),
(4,'Bambang',3,3,'Alamat Siswa'),
(5,'Fitri',2,4,'Alamat Siswa'),
(6,'Bagus',1,5,'Alamat Siswa'),
(7,'Hartoko',1,5,'Alamat Siswa'),
(8,'Dadan',3,6,'Alamat Siswa'),
(9,'Ceceng',2,7,'Alamat Siswa'),
(10,'Ilham',1,1,'Alamat Siswa'),
(11,'Iqbal',3,8,'Alamat Siswa'),
(12,'Adi',2,4,'Alamat Siswa');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
