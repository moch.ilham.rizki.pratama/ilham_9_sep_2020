<?php defined('BASEPATH') OR exit ('No direct script access allowed');

class DataModel extends CI_Model{

    //ambil data siswa dari database
    function get_siswa_list(){
        // $query = $this->db->get('siswa');
        // return $query->result();
        $sql = $this->db->query('SELECT siswa.`id_siswa`, siswa.`nama_siswa`, kota.`nama_kota`, kecamatan.`nama_kecamatan`, siswa.`alamat` FROM siswa, kota, kecamatan 
        WHERE kota.`id_kota`=siswa.`id_kota` AND kecamatan.`id_kecamatan`=siswa.`id_kecamatan`');
        return $sql->result();
    }
    
    //ambil data kecamatan dari database
    function get_kecamatan_list(){
        // $query = $this->db->get('kecamatan');
        // return $query->result();
        $sql = $this->db->query('SELECT kecamatan.`id_kecamatan`, kecamatan.`nama_kecamatan`, kota.`nama_kota` FROM kecamatan, kota 
        WHERE kota.`id_kota`=kecamatan.`id_kota`');
        return $sql->result();
    }

    //ambil data Kota / Kabupaten dari database
    function get_kota_list(){
        $query = $this->db->get('kota');
        return $query->result();
    }
    public function siswa(){
        $query = $this->db->query("SELECT * FROM siswa");
        return $query->result();
    }
	
	public function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	
	public function edit_siswa($where,$table){
		return $this->db->get_where($table,$where);
    }
    
    public function edit_kecamatan($where,$table){
		return $this->db->get_where($table,$where);
    }
    
	public function edit_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	public function hapus_siswa($id){
		$this->db->where('id_siswa',$id);
		$this->db->delete('siswa');
		return;
	}
}