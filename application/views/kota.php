        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-12 mb-4">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Data Kota / Kabupaten</h6>
                </div>
                <div class="card-body"> 
					
				<div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id Kota / Kabupaten</th>
                      <th>Kota / Kabupaten</th>
                      <!-- <th><button href="<?php echo base_url() ?>" type="submit" class="btn btn-success" >Tambah</button></th> -->
                    </tr>
                  </thead>
                  <tbody>
					<?php foreach ($kota as $kota): ?>
						<tr>
							<td width="150">
								<?php echo $kota->id_kota ?>
							</td>
							<td>
								<?php echo $kota->nama_kota ?>
							</td>
							<td>
								<a href="<?php echo site_url('admin/kota/edit/'.$kota->id_kota) ?>"
									class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
								<a onclick="deleteConfirm('<?php echo site_url('admin/kota/delete/'.$kota->id_kota) ?>')"
									href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
							</td>
						</tr>
						<?php endforeach; ?>
				</table>
				</div>
				</div>
              </div>

            <div class="col-lg-6 mb-4">

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
